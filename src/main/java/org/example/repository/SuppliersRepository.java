package org.example.repository;

import org.example.entities.Suppliers;
import org.hibernate.SessionFactory;

public class SuppliersRepository extends GenericAbstractRepository <Suppliers>{

    public SuppliersRepository(SessionFactory sessionFactory){
        super (sessionFactory);
    }
    @Override
    public String getEntityName() {
        return "Suppliers";
    }

    @Override
    public Class<Suppliers> getEntityClass() {
        return Suppliers.class;
    }
}
