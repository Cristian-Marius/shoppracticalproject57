package org.example.repository;

import org.example.entities.Employee;
import org.example.entities.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class ProductRepository extends GenericAbstractRepository <Product> {
    public  ProductRepository(SessionFactory sessionFactory){
        super(sessionFactory);
    }
    @Override
    public String getEntityName() {
        return "Product";
    }

    @Override
    public Class<Product> getEntityClass() {
        return Product.class;
    }
//    private SessionFactory sessionFactory;
//
//    public ProductRepository(SessionFactory sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }
//
//    public Product createProduct(Product product) {
//        Session session = sessionFactory.openSession();
//        Transaction transaction = session.beginTransaction();
//        session.persist(product);
//        transaction.commit();
//        session.close();
//        return product;
//    }
//
//    public Product readById(Integer id) {
//        Session session = sessionFactory.openSession();
//        Product product = session.find(Product.class, id);
//        session.close();
//        return product;
//    }
//    public Product updateProduct(Product p) {
//        Session session = sessionFactory.openSession();
//        Transaction transaction = session.beginTransaction();
//        p = session.merge(p);
//        transaction.commit();
//        session.close();
//        return p;
//    }
}
