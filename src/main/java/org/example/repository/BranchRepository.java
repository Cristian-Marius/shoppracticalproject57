package org.example.repository;

import org.example.entities.Branch;
import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class BranchRepository extends GenericAbstractRepository<Branch> {
    public BranchRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public String getEntityName() {
        return null;
    }

    @Override
    public Class<Branch> getEntityClass() {
        return null;
    }

    public String gerEntityName(){

        return "Branch";

}
//    private SessionFactory sessionFactory;
//
//    public BranchRepository(SessionFactory sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }
//    public Branch createBranch(Branch branch){
//        Session session = sessionFactory.openSession();
//        Transaction transaction = session.beginTransaction();
//        session.persist(branch);
//        transaction.commit();
//        session.close();
//        return branch;
//    }
//    public Branch readById(Integer id){
//        Session session = sessionFactory.openSession();
//        Branch branch=session.find(Branch.class, id);
//        session.close();
//        return branch;
//    }
//
//    public List<Branch> readALL (){
//        Session session = sessionFactory.openSession();
//        List<Branch> branchList = session.createQuery("select b from Branch b ", Branch.class).getResultList();
//        session.close();
//        return(branchList);
//    }
//    public Branch updateBranchDetails(Branch b){
//        Session session= sessionFactory.openSession();
//        Transaction transaction= session.beginTransaction();
//        b = session.merge(b);
//        transaction.commit();
//        session.close();
//        return b;
//    }
//
//
//    public void deleteBranch(Branch branch){
//        Session session= sessionFactory.openSession();
//        Transaction transaction= session.beginTransaction();
//        session.remove(branch);
//        transaction.commit();
//        session.close();
//    }
//


}

