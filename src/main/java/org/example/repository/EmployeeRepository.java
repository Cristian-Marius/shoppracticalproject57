package org.example.repository;


import org.example.entities.Branch;
import org.example.entities.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class EmployeeRepository extends GenericAbstractRepository <Employee>{
     public EmployeeRepository(SessionFactory sessionFactory){
         super(sessionFactory);
     }

    @Override
    public String getEntityName() {
        return "Employee";
    }

    @Override
    public Class<Employee> getEntityClass() {
        return Employee.class;
    }


//    private SessionFactory sessionFactory;
//
//
//    public EmployeeRepository(SessionFactory sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }
//
//    public Employee createEmployee(Employee employee) {
//        Session session = sessionFactory.openSession();
//        Transaction transaction = session.beginTransaction();
//        session.persist(employee);
//        transaction.commit();
//        session.close();
//        return employee;
//    }
//
//    public Employee readById(Integer id) {
//        Session session = sessionFactory.openSession();
//        Employee employee = session.find(Employee.class, id);
//        session.close();
//        return employee;
//    }
//
//    public Employee updateEmployee(Employee e) {
//        Session session = sessionFactory.openSession();
//        Transaction transaction = session.beginTransaction();
//        e = session.merge(e);
//        transaction.commit();
//        session.close();
//        return e;
//    }
//    public void deleteEmployee(Employee employee){
//        Session session= sessionFactory.openSession();
//        Transaction transaction= session.beginTransaction();
//        session.remove(employee);
//        transaction.commit();
//        session.close();
//    }
}
