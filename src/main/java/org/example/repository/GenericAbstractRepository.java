package org.example.repository;

import org.example.entities.Branch;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public abstract class GenericAbstractRepository<T> {
    private SessionFactory sessionFactory;

    public GenericAbstractRepository(SessionFactory sessionFactory){
        this.sessionFactory= sessionFactory;
    }
    public T create(T t){

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(t);
        transaction.commit();
        session.close();
        return t;
    }
    public T readBy(Integer id){
        Session session = sessionFactory.openSession();
        T t=session.find(getEntityClass(), id);
        session.close();
        return t;
    }

    public List <T> readAll(){
        Session session = sessionFactory.openSession();
        List<T> tList = session.createQuery("select t from " + getEntityName() +  " t ", getEntityClass()).getResultList();
        session.close();
        return(tList);
    }
    public T updateDetails(T t){
        Session session= sessionFactory.openSession();
        Transaction transaction= session.beginTransaction();
        t= session.merge(t);
        session.close();
        return t;
    }

    public void delete(T t){
        Session session= sessionFactory.openSession();
        Transaction transaction= session.beginTransaction();
        session.remove(t);
        transaction.commit();
        session.close();
    }

    public  abstract String getEntityName();
    public abstract  Class<T> getEntityClass();
}
