package org.example.repository;

import org.example.entities.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class ClientRepository extends GenericAbstractRepository <Client> {

    public ClientRepository(SessionFactory sessionFactory){
        super(sessionFactory);
    }
    @Override
    public String getEntityName() {
        return "Client";
    }

    @Override
    public Class<Client> getEntityClass() {
        return Client.class;
    }


//    private SessionFactory sessionFactory;
//
//    public ClientRepository(SessionFactory sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }
//    public Client createClient(Client Client){
//        Session session = sessionFactory.openSession();
//        Transaction transaction = session.beginTransaction();
//        session.persist(Client);
//        transaction.commit();
//        session.close();
//        return Client;
//    }
//    public Client readById(Integer id){
//        Session session = sessionFactory.openSession();
//        Client Client=session.find(Client.class, id);
//        session.close();
//        return Client;
//    }
//
//    public List<Client> readALL (){
//        Session session = sessionFactory.openSession();
//        List<Client> ClientList = session.createQuery("select b from Client b ", Client.class).getResultList();
//        session.close();
//        return(ClientList);
//    }
//    public Client updateClientDetails(Client b){
//        Session session= sessionFactory.openSession();
//        Transaction transaction= session.beginTransaction();
//        b = session.merge(b);
//        transaction.commit();
//        session.close();
//        return b;
//    }
//
//
//    public void deleteClient(Client Client){
//        Session session= sessionFactory.openSession();
//        Transaction transaction= session.beginTransaction();
//        session.remove(Client);
//        transaction.commit();
//        session.close();
//    }

}
