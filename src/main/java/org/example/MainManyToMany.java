package org.example.repository;

import org.example.config.HibernateConfiguration;
import org.example.entities.Category;
import org.hibernate.SessionFactory;

public class MainManyToMany {

    public static void main(String[] args) {
        CategoryRepository categoryRepository = new CategoryRepository(HibernateConfiguration.getSessionFactory());

        Category category1= new Category(null,"Lapte Bio",false);
        Category category2= new Category(null,"Lapte Eco",false);
        Category category3= new Category(null,"Alcool",true);

        categoryRepository.create(category1);
        categoryRepository.create(category2);
        categoryRepository.create(category3);
    }
}
