package org.example.ex.generics;

public class ZaharDataHandler extends GenericDataHandler<Zahar> {
    public String comp(Zahar f1, Zahar f2) {
        if (f1.pret < f2.pret) {
            return "a doua e mai buna";
        } else {
            return "prima e mai buna";
        }
    }
}