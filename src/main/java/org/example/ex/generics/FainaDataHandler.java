package org.example.ex.generics;

 //fiecare clasa normala care extinda O clasa abstracta trebuie sa implementeze metodele abstracte de parinte

public class FainaDataHandler extends GenericDataHandler<Faina>{
    public String comp(Faina f1, Faina f2){
        if(f1.pret< f2.pret){
            return "a doua e mai buna";
        }else{
            return "prima e mai buna";
        }
    }
}
