package org.example.ex.generics;

public class LapteDataHandler extends GenericDataHandler<Lapte> {
    public String comp(Lapte f1, Lapte f2) {
        if (f1.pret < f2.pret) {
            return "a doua e mai buna";
        } else {
            return "prima e mai buna";
        }
    }
}