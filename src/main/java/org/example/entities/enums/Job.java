package org.example.entities.enums;

public enum Job {

    CASHIER, MANAGER, OTHER
}
