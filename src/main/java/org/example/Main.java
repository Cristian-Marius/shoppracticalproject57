package org.example;

import org.example.config.HibernateConfiguration;
import org.example.entities.Branch;
import org.example.entities.Employee;
import org.example.entities.enums.Job;
import org.example.repository.BranchRepository;
import org.example.repository.EmployeeRepository;
import org.hibernate.SessionFactory;

import java.util.List;


public class Main {
    public static void main(String[] args) {

        System.out.println("Trying to open session factory");
        SessionFactory sessionFactory= HibernateConfiguration.getSessionFactory();
        System.out.println("Session created");

        BranchRepository branchRepository = new BranchRepository(sessionFactory);
        Branch branch1 = new Branch(null,"Mega Image","Titulescu");
        Branch branch2 = new Branch(null,"Auchan","Centura");
        Branch branch3 = new Branch(null,"Profi","Transilvaniei");
        Branch branch4 = new Branch(null,"Lidl","Borsului");
        branchRepository.createBranch(branch1);
        branchRepository.createBranch(branch2);
        branchRepository.createBranch(branch3);
        branchRepository.createBranch(branch4);


        Branch readBranch =  branchRepository.readById(2);
        System.out.println(readBranch);

        List<Branch> readAllBranchList =branchRepository.readALL();
        System.out.println(readBranch);

        branch2.setName("Auchan");
        branchRepository.updateBranchDetails(branch2);

        branchRepository.deleteBranch(branch1);
        System.out.println(branchRepository.readALL());

        EmployeeRepository employeeRepository= new EmployeeRepository(sessionFactory);
        Employee employee1= new Employee(null,"Buda Cristian", Job.CASHIER);
        Employee employee2= new Employee(null,"Marius Dumitrache",Job.MANAGER);
        Employee employee3= new Employee(null,"Marian Frizeru",Job.OTHER);
        Employee employee4= new Employee(null,"Ombladon Cheloo",Job.CASHIER);

        employeeRepository.createEmployee(employee1);
        employeeRepository.createEmployee(employee2);
        employeeRepository.createEmployee(employee3);
        employeeRepository.createEmployee(employee4);

        Employee reademployee =  employeeRepository.readById(2);
        System.out.println(reademployee);

        employee2.setName("Gheorghe Hagi");
        employeeRepository.updateEmployee(employee2);

        employeeRepository.deleteEmployee(employee3);


    }
}