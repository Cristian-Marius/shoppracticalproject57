package org.example.config;

import org.example.entities.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateConfiguration {
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            sessionFactory = new Configuration()
                    .configure("hibernarte.config.xml")
                    .addAnnotatedClass(Product.class)
                    .addAnnotatedClass(Suppliers.class)
                    .addAnnotatedClass(Branch.class)
                    .addAnnotatedClass(Employee.class)
                    .addAnnotatedClass(Client.class)
                    .buildSessionFactory();
        }
        return sessionFactory;
    }
}
